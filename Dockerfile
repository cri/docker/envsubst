FROM registry.cri.epita.fr/cri/docker/mirror/debian:10

ENV DEBIAN_FRONTEND=noninteractive \
    LC_ALL=C.UTF-8 \
    LANG=C.UTF-8

RUN apt-get update && \
    apt-get install -y --no-install-recommends \
        curl ca-certificates gnupg apt-transport-https \
        gettext

RUN curl -1sLf https://github.com/a8m/envsubst/releases/download/v1.2.0/envsubst-Linux-x86_64 -o /usr/local/bin/go-envsubst && \
    chmod +x /usr/local/bin/go-envsubst

ENTRYPOINT ["/bin/sh", "-c"]
